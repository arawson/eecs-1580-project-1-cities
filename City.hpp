/* 
 * @file:   City.hpp
 * @author: Aaron Rawson
 * 
 * Created on January 24, 2013, 11:58 AM
 */

#pragma once

#include <string>
#include <iostream>

using namespace std;

class City {
public:
    City();
    City(string info_line);

    int get_id() {
        return id;
    }

    int get_state_code() {
        return state_code;
    }
    
    int get_county_code() {
        return county_code;
    }

    string get_classification() {
        return classification;
    }

    string get_county() {
        return county;
    }

    double get_latitude() {
        return latitude;
    }

    double get_longitude() {
        return longitude;
    }

    int get_elevation() {
        return elevation;
    }

    int get_population() {
        return population;
    }

    string get_map_name() {
        return map_name;
    }
    
    string get_state() {
        return state;
    }
    
    string get_name() {
        return name;
    }
    
    string to_string() const;

private:
    int id;
    int state_code;
    int county_code;
    string state;
    string name;
    string classification;
    string county;
    double latitude;
    double longitude;
    int elevation;
    int population;
    string map_name;
    enum FieldNumbers {
        ID = 0,
        STATE = 1,
        NAME = 2,
        CLASS = 3,
        COUNTY = 4,
        STATE_CODE = 5,
        COUNTY_CODE = 6,
        LATITUDE_DMS = 7,
        LONGITUDE_DMS = 8,
        LATITUDE_DECIMAL = 9,
        LONGITUDE_DECIMAL = 10,
        ELEVATION = 15,
        POPULATION = 16,
        MAP_NAME = 18
    };
};

typedef bool (*CityComparator)(City,City);

void sort(City input[], int length, CityComparator compfn);

bool byStateAndCity(City a, City b);

bool byLongitude(City a, City b);

ostream& operator<<(ostream &strm, const City &c);
