/* 
 * File:   main.cpp
 * Author: Aaron Rawson
 *
 * Created on January 17, 2013, 9:41 AM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include "City.hpp"

using namespace std;

const int MAX_CITIES = 1000;

void writeOut(string name, City cities[], int length);

int main(int argc, char** argv) {
    ifstream inFile;
    string input_name;

    cout << "Enter name of file to read: ";
    cin >> input_name;

    inFile.open(input_name.c_str(), ifstream::in);

    if (!inFile.good()) {
        cout << "could not open file: " << input_name << endl;
        return EXIT_FAILURE;
    }

    City cities[MAX_CITIES];
    int how_many= 0;

    for (int i = 0; i < MAX_CITIES; i++) {
        string line;

        getline(inFile, line);

        if (line.length() > 0) {
            cities[i] = City(line);
        }

        if (inFile.eof()) {
            break;
        }
        how_many ++;
    }
    
    cout << "Read in " << how_many << " cities." << endl;

    inFile.close();
    
    sort(cities, how_many, &byStateAndCity);
    writeOut("byCity.txt", cities, how_many);
    cout << "The cities have been sorted by state and name and put in \"byCity.txt\"" << endl;
    sort(cities, how_many, &byLongitude);
    writeOut("byLongitude.txt", cities, how_many);
    cout << "The cities have been sorted by longitude and put in \"byLongitude.txt\"" << endl;

    return 0;
}

void writeOut(string name, City cities[], int length) {
    ofstream output(name.c_str(), ifstream::trunc);
    
    if (!output.good()) {
        cout << "Could not open file \"" << name << "\" for writing!" << endl;
        return;
    }
    
    for (int i = 0; i < length; i++) {
        output << cities[i] << endl;
    }
    output.close();
}
