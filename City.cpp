/* 
 * @file:   City.cpp
 * @author: Aaron Rawson
 * 
 * Created on January 24, 2013, 11:58 AM
 */

#include "City.hpp"
#include <sstream>
#include <iostream>

City::City(string info_line) : classification(""), county(""), county_code(0), elevation(0), id(0),
    latitude(0), longitude(0), map_name(""), name(""),population(0), state(""), state_code(0) {
    
    int current_index = 0;
    int delta = 0;
    for (int field = 0; field <= 18; field++) {
        unsigned int seperator_index = info_line.find('|', current_index);
        
        if (seperator_index != string::npos) {
            delta = seperator_index - current_index;
        } else {
            delta = info_line.length() - current_index - 1;
        }
        
        string sub_string = info_line.substr(current_index, delta);
        
        if (sub_string.length() > 0) {
        switch (field) {
            case CLASS: {
                classification = sub_string;
                break;
            }
            case COUNTY: {
                county = sub_string;
                break;
            }
            case COUNTY_CODE: {
                istringstream(sub_string) >> county_code;
                break;
            }
            case ELEVATION: {
                istringstream(sub_string) >> elevation;
                break;
            }
            case ID: {
                istringstream(sub_string) >> id;
                break;
            }
            case LATITUDE_DECIMAL: {
                istringstream(sub_string) >> latitude;
                break;
            }
            case LONGITUDE_DECIMAL: {
                istringstream(sub_string) >> longitude;
                break;
            }
            case MAP_NAME: {
                map_name = sub_string;
                break;
            }
            case NAME: {
                name = sub_string;
                break;
            }
            case POPULATION: {
                istringstream(sub_string) >> population;
                break;
            }
            case STATE: {
                state = sub_string;
                break;
            }
            case STATE_CODE: {
                istringstream(sub_string) >> state_code;
                break;
            }
        }
        }
        
        current_index += delta + 1;
    }
    
}

City::City() {
    id = -1;
}

string City::to_string() const {
    stringstream ss;
    ss << "City: " << name;
    ss <<  "   ";
    ss << "State: " << state;
    ss <<  "   ";
    ss << "County: " << county;
    ss <<  "   ";
    ss << "Id: " << id;
    ss <<  "   ";
    ss << "Latitude: " << latitude;
    ss <<  "   ";
    ss << "Longitude: " << longitude;
    ss <<  "   ";
    ss << "Population: " << population;
    ss <<  "   ";
    ss << "Elevation: " << elevation;
    return ss.str();
}

void sort(City input[], int length, CityComparator compfn) {
    City temp;
    int  j;
    for (int i = 1; i < length; i++) {
        j = i;
        while (j > 0 && compfn(input[j-1], input[j])) {
            temp = input[j];
            input[j] = input[j -1];
            input[j - 1] = temp;
            j--;
        }
    }
}

bool byStateAndCity(City a, City b) {
    return (!a.get_state().compare(b.get_state()) < 0) && (!a.get_name().compare(b.get_name()) < 0);
}

bool byLongitude(City a, City b) {
    return a.get_longitude() > b.get_longitude();
}

ostream& operator<<(ostream &strm, const City &c) {
    return strm << c.to_string();
}